with import <nixpkgs> {}; {
  sdlEnv = stdenv.mkDerivation {
    name = "xscreensaver";
    buildInputs =
      [ pkgconfig bc perl libjpeg libGLU libGL gtk2 libxml2 gnome2.libglade pam
        xorg.libXext xorg.libXScrnSaver xorg.libX11 xorg.libXrandr xorg.libXmu xorg.libXxf86vm xorg.libXrender
        xorg.libXxf86misc intltool xorg.appres makeWrapper gle
      ];
  };
}
